//
//  ViewController.m
//  p01-novitske
//
//  Created by Steven Novitske on 1/27/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
- (IBAction)buttonClick;
@property (weak, nonatomic) IBOutlet UILabel *label1;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick {
    _label1.text = @"Steven Novitske";
}
@end
