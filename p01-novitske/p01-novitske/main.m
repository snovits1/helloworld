//
//  main.m
//  p01-novitske
//
//  Created by Steven Novitske on 1/27/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
